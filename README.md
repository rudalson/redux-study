# redux-study

[생활코딩의 Redux](https://www.youtube.com/playlist?list=PLuHgQVnccGMB-iGMgONoRPArZfjRuRNVc) 학습


### 빈개체에 값넣기
```shell script
Object.assign({}, {name:'rudalson'}, {city:'pusan'});
{name:"rudalson", city:"pusan")
```


## Redux Dev Tools 설치
[redux-devtools-extension](https://github.com/zalmoxisus/redux-devtools-extension) 을 설치해준다.

